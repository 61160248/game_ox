
import java.util.Scanner;

public class OXgit {

    static final int row = 3, col = 3;
    static int[][] board = new int[row][col];
    static int now_player;
    static int now_row, now_col;
    static int state;

    public static void showWelcome() {
        System.out.println("Welcome To OX Game");
    }

    public static void showBye() {
        System.out.println("Bye Bye...");
    }

    public static void showBoard() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                showChoice(board[i][j]);
                if (j != col - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i != row - 1) {
                System.out.println("-----------");
            }
        }
        System.out.println();
    }

    public static void showChoice(int choice) {
        final int empty = 0, o = 2, x = 1;
        switch (choice) {
            case empty:
                System.out.print("   ");
                break;
            case o:
                System.out.print(" O ");
                break;
            case x:
                System.out.print(" X ");
                break;
        }
    }

    public static void showTurn(int choice) {
        final int x = 1;
        if (choice == x) {
            System.out.println("Turn X");
        } else {
            System.out.println("Turn O");
        }
    }

    public static void input(int choice) {
        boolean check = false;
        final int empty = 0;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Input your Row Col :");
            int i = sc.nextInt() - 1, j = sc.nextInt() - 1;
            if (i >= 0 && i < row && j >= 0 && j < col && board[i][j] == empty) {
                now_row = i;
                now_col = j;
                board[now_row][now_col] = choice;
                check = true;
            } else {
                System.out.println("Input try again...");
            }
        } while (!check);
    }

    public static boolean checkWin(int choice, int row, int col) {
        return (board[row][0] == choice
                && board[row][1] == choice
                && board[row][2] == choice
                || board[0][col] == choice
                && board[1][col] == choice
                && board[2][col] == choice
                || row == col
                && board[0][0] == choice
                && board[1][1] == choice
                && board[2][2] == choice
                || row + col == 2
                && board[0][2] == choice
                && board[1][1] == choice
                && board[2][0] == choice);
    }

    public static boolean checkDraw() {
        final int empty = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == empty) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void update(int choice, int row, int col) {
        final int x = 1, draw = 1, x_win = 2, o_win = 3;
        if (checkWin(choice, row, col)) {
            state = (choice == x) ? x_win : o_win;
        } else if (checkDraw()) {
            state = draw;
        }
    }

    public static void switchPlayer() {
        final int x = 1, o = 2;
        now_player = (now_player == x) ? o : x;
    }

    public static void inGame() {
        int state;
        final int play = 0, empty = 0, x = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                board[i][j] = empty;
            }
        }
        state = play;
        now_player = x;
    }

    public static void showWin() {
        final int x = 1, draw = 1, x_win = 2, o_win = 3;
        if (state == x_win) {
            System.out.println("Player 'X' WIN!");
        } else if (state == o_win) {
            System.out.println("Player 'O' WIN!");
        } else if (state == draw) {
            System.out.println("Draw!");
        }
    }

    public static void main(String[] args) {
        final int play = 0;
        inGame();
        showWelcome();
        do {
            showBoard();
            showTurn(now_player);
            input(now_player);
            update(now_player, now_row, now_col);
            switchPlayer();
        } while (state == play);
        showBoard();
        showWin();
        showBye();
    }

}
